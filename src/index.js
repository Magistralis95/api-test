const express = require('express');
const app = express();
//const morgan= require('morgan');

//Settings
app.set('port', process.env.PORT || 443); // algunos servicios entregan puertos por defecto como azure
//app.set('jason spaces', 2);

//Middlewares //es una peticion que procesa datos antes que la reciba
//app.use(morgan('dev'));
//app.use(express.urlencoded({extended: false}));//permite enterder datos de input de formularios
app.use(express.json());//este metodo permite recibir json y entenderlos 

//ROUTES
app.use(require('./routes/token'));

//Starting the server
app.listen(app.get('port'), () =>{
    console.log(`Server on port ${app.get('port')}`);
});